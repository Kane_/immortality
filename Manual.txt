Immortality: Player Manual
--------------------------
    
    Revision History:
    =================
	
		bugfix #1:
			after self-testing
				- fixed some damages issues
				- make the end screen more clear
    
		bugfix #2:
			after [Mirroar] request
				- added display indication for helping know when you are standing on a trap
    
		bugfix #3:
			after friend testing
				- fixed a fire trap bug
    
    History:
    ========
    
    You are an immortal, you can't die, and you want to get rid of that
    "curse". To do that, you have to go at the last floor of the Temple
    of Eternity and take the Rune of Mortality.
    
    Good thing for you, the temple is not guarded, and even if it was,
    you do not care ! After all you are immortal !
    But the temple is full of deadly trap waiting only to harm you.
    Since you can not die, you are not so worried, but are you really sure
	not to do so? Think of it! You only get one body!
    
    Can you reach the Rune of Mortality ? And even more, Can you succeed to
    leave the temple after get the Rune ! We will see :)
    
    Commands:
    =========
    
    to move in the four directions you can use arrows key, zqsd, wasd or hjkl
    
              z        w       j
            q-|-d    a-|-s   h-|-l
              s        d       k
    
    to go downstair you have to press the [>] key on the same tile.
    
    to go upstair you have to press the [<] key on the same tile.
    note that you can't leave the temple without being mortal.
    
    to drink a potion that will restore your health use the [p] key.
    
    to use a bandage that will stop every damage use the [b] key.
    
    to quit the game use [escape] key.
    
    
    Hints:
    ======
    
    - To drink a potion you need at least your head
    
    - To use a bandage you need at least one arm

	- Do not use potions/bandages when you are standing on a trap, that will reactive it!
	
	- Take your time, if you do not move, the game is in a pause state!
	
	- The game have two ends !