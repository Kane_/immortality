#ifndef HAVE_DISPLAY_H
#define HAVE_DISPLAY_H

#include <curses.h>
#include <panel.h>

#include "player.h"

char log_line[60];

void display_init(void);
void display_init_colors(void);
void display_destroy(void);

void display_title(void);
void display_interface(void);
void display_log(void);
void display_map(char (*map)[18][50]);
void display_player(player_t *player);

void display_good_ending(player_t *player);
void display_bad_ending(player_t *player);

#endif
