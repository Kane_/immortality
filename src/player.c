#include <stdlib.h>
#include <stdio.h>

#include "player.h"

player_t *player_init(void) {
    player_t *player = NULL;
    
    player = calloc(1, sizeof(player_t));
    player->current_level = &level_01;
    player->mortal = 0;
    player->level = 1;
    player->score = 0;
    player->potions = 2;
    player->bandages = 2;
    player->posY = 5;
    player->posX = 5;
    player->head.health = 100;
    player->body.health = 100;
    player->l_arm.health = 100;
    player->r_arm.health = 100;
    player->l_leg.health = 100;
    player->r_leg.health = 100;

    player->head.state = 'o';
    player->body.state = 'o';
    player->l_arm.state = 'o';
    player->r_arm.state = 'o';
    player->l_leg.state = 'o';
    player->r_leg.state = 'o';
    
    return player;
}

void player_destroy(player_t **player) {
    free(*player);
}