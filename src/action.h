#ifndef HAVE_ACTION_H
#define HAVE_ACTION_H

#include "player.h"
#include "display.h"

void walk_up(player_t *player);
void walk_left(player_t *player);
void walk_right(player_t *player);
void walk_down(player_t *player);

void drink_potion(player_t *player);
void use_bandage(player_t *player);

void go_downstair(player_t *player);
void go_upstair(player_t *player);

void check_floor(player_t *player);

void get_poisoned(player_t *player);
void get_burned(player_t *player);
void get_pierced(player_t *player);
void get_acid_burn(player_t *player);
void fall(player_t *player);

void take_poison_damage(player_t *player);
void take_fire_damage(player_t *player);
void take_spear_damage(player_t *player);
void take_acid_damage(player_t *player);
void take_fall_damage(player_t *player);

void check_ending(player_t *player);
void set_log(const char *text);

#endif
