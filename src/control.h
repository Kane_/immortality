#ifndef HAVE_CONTROL_H
#define HAVE_CONTROL_H

#include <curses.h>

#include "player.h"
#include "action.h"

int key_event(player_t *player, int key);

#endif
