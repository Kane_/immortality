#include <stdlib.h>
#include <string.h>

#include "display.h"

void display_init(void) {
    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    curs_set(0);
}

void display_init_colors(void) {
    start_color();
    
    init_pair( 1, COLOR_BLACK, COLOR_BLACK);
    init_pair( 2, COLOR_BLACK, COLOR_RED);
    init_pair( 3, COLOR_BLACK, COLOR_GREEN);
    init_pair( 4, COLOR_BLACK, COLOR_BLUE);
    init_pair( 5, COLOR_BLACK, COLOR_CYAN);
    init_pair( 6, COLOR_BLACK, COLOR_MAGENTA);
    init_pair( 7, COLOR_BLACK, COLOR_YELLOW);
    init_pair( 8, COLOR_BLACK, COLOR_WHITE);
    
    init_pair( 9, COLOR_RED, COLOR_BLACK);
    init_pair(10, COLOR_RED, COLOR_RED);
    init_pair(11, COLOR_RED, COLOR_GREEN);
    init_pair(12, COLOR_RED, COLOR_BLUE);
    init_pair(13, COLOR_RED, COLOR_CYAN);
    init_pair(14, COLOR_RED, COLOR_MAGENTA);
    init_pair(15, COLOR_RED, COLOR_YELLOW);
    init_pair(16, COLOR_RED, COLOR_WHITE);
    
    init_pair(17, COLOR_GREEN, COLOR_BLACK);
    init_pair(18, COLOR_GREEN, COLOR_RED);
    init_pair(19, COLOR_GREEN, COLOR_GREEN);
    init_pair(20, COLOR_GREEN, COLOR_BLUE);
    init_pair(21, COLOR_GREEN, COLOR_CYAN);
    init_pair(22, COLOR_GREEN, COLOR_MAGENTA);
    init_pair(23, COLOR_GREEN, COLOR_YELLOW);
    init_pair(24, COLOR_GREEN, COLOR_WHITE);
    
    init_pair(25, COLOR_BLUE, COLOR_BLACK);
    init_pair(26, COLOR_BLUE, COLOR_RED);
    init_pair(27, COLOR_BLUE, COLOR_GREEN);
    init_pair(28, COLOR_BLUE, COLOR_BLUE);
    init_pair(29, COLOR_BLUE, COLOR_CYAN);
    init_pair(30, COLOR_BLUE, COLOR_MAGENTA);
    init_pair(31, COLOR_BLUE, COLOR_YELLOW);
    init_pair(32, COLOR_BLUE, COLOR_WHITE);
    
    init_pair(33, COLOR_CYAN, COLOR_BLACK);
    init_pair(34, COLOR_CYAN, COLOR_RED);
    init_pair(35, COLOR_CYAN, COLOR_GREEN);
    init_pair(36, COLOR_CYAN, COLOR_BLUE);
    init_pair(37, COLOR_CYAN, COLOR_CYAN);
    init_pair(38, COLOR_CYAN, COLOR_MAGENTA);
    init_pair(39, COLOR_CYAN, COLOR_YELLOW);
    init_pair(40, COLOR_CYAN, COLOR_WHITE);
    
    init_pair(41, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(42, COLOR_MAGENTA, COLOR_RED);
    init_pair(43, COLOR_MAGENTA, COLOR_GREEN);
    init_pair(44, COLOR_MAGENTA, COLOR_BLUE);
    init_pair(45, COLOR_MAGENTA, COLOR_CYAN);
    init_pair(46, COLOR_MAGENTA, COLOR_MAGENTA);
    init_pair(47, COLOR_MAGENTA, COLOR_YELLOW);
    init_pair(48, COLOR_MAGENTA, COLOR_WHITE);
    
    init_pair(49, COLOR_YELLOW, COLOR_BLACK);
    init_pair(50, COLOR_YELLOW, COLOR_RED);
    init_pair(51, COLOR_YELLOW, COLOR_GREEN);
    init_pair(52, COLOR_YELLOW, COLOR_BLUE);
    init_pair(53, COLOR_YELLOW, COLOR_CYAN);
    init_pair(54, COLOR_YELLOW, COLOR_MAGENTA);
    init_pair(55, COLOR_YELLOW, COLOR_YELLOW);
    init_pair(56, COLOR_YELLOW, COLOR_WHITE);
    
    init_pair(57, COLOR_WHITE, COLOR_BLACK);
    init_pair(58, COLOR_WHITE, COLOR_RED);
    init_pair(59, COLOR_WHITE, COLOR_GREEN);
    init_pair(60, COLOR_WHITE, COLOR_BLUE);
    init_pair(61, COLOR_WHITE, COLOR_CYAN);
    init_pair(62, COLOR_WHITE, COLOR_MAGENTA);
    init_pair(63, COLOR_WHITE, COLOR_YELLOW);
    init_pair(64, COLOR_WHITE, COLOR_WHITE);
}

void display_destroy(void) {
    getch();
    curs_set(1);
    keypad(stdscr, FALSE);
    echo();
    nocbreak();
    endwin();
}

void display_title(void) {
    attron(COLOR_PAIR(17));
    mvwprintw(stdscr, 2, 2, "Immortality - A Ludum Dare game");
    attroff(COLOR_PAIR(17));
    
    mvwprintw(stdscr, LINES / 2, COLS / 2 - strlen("Press <any> key to start")/2, "Press <any> key to start");
    
    mvwprintw(stdscr, LINES - 2, COLS - (strlen("By Kane <yarg.fr>") + 2), "By Kane <yarg.fr>");
}

void display_interface(void) {
    register int y, x;
    
    clear();
    for (y = 0; y < LINES; y++) {
        for (x = 0; x < COLS; x++) {
            if ((x >= COLS - 53 && y >= 1) && (x <= COLS - 2 && y <= 20)) {
                attron(COLOR_PAIR(8));
                    mvwaddch(stdscr, y, x, ' ');
                attroff(COLOR_PAIR(8));
            }
        }
    }
}

void display_log(void) {
    mvwprintw(stdscr, 21, COLS - 52, "%s", log_line);
}
void display_map(char (*map)[18][50]) {
    register int y, x;
    
    for (y = 2; y < 20; y++) {
        for (x = COLS - 52; x < COLS - 2; x++) {
            switch ((*map)[y - 2][x - (COLS - 52)]) {
                case '0':
                    mvwaddch(stdscr, y, x, ' ');
                    break;
                case '1':
                    mvwaddch(stdscr, y, x, '#');
                    break;
                case '2':
                    attron(COLOR_PAIR(25));
                    mvwaddch(stdscr, y, x, '~');
                    attroff(COLOR_PAIR(25));
                    break;
                case '3':
                    mvwaddch(stdscr, y, x, '.');
                    break;
                case '4':
                    mvwaddch(stdscr, y, x, '.');
                    break;
                case '5':
                    mvwaddch(stdscr, y, x, '.');
                    break;
                case '6':
                    mvwaddch(stdscr, y, x, '.');
                    break;
                case '7':
                    mvwaddch(stdscr, y, x, '.');
                    break;
                case '8':
                    mvwaddch(stdscr, y, x, '.');
                    break;
                case '9':
                    mvwaddch(stdscr, y, x, '.');
                    break;
                case 'A':
                    attron(COLOR_PAIR(17));
                    mvwaddch(stdscr, y, x, '^');
                    attroff(COLOR_PAIR(17));
                    break;
                case 'B':
                    attron(COLOR_PAIR(9));
                    mvwaddch(stdscr, y, x, '^');
                    attroff(COLOR_PAIR(9));
                    break;
                case 'C':
                    mvwaddch(stdscr, y, x, '^');
                    break;
                case 'D':
                    attron(COLOR_PAIR(33));
                    mvwaddch(stdscr, y, x, '^');
                    attroff(COLOR_PAIR(33));
                    break;
                case 'E':
                    attron(COLOR_PAIR(8));
                    mvwaddch(stdscr, y, x, ' ');
                    attroff(COLOR_PAIR(8));
                    break;
                case '$':
                    attron(COLOR_PAIR(49));
                    mvwaddch(stdscr, y, x, '$');
                    attroff(COLOR_PAIR(49));
                    break;
                case '!':
                    attron(COLOR_PAIR(33));
                    mvwaddch(stdscr, y, x, '!');
                    attroff(COLOR_PAIR(33));
                    break;
                case '=':
                    attron(COLOR_PAIR(17));
                    mvwaddch(stdscr, y, x, '=');
                    attroff(COLOR_PAIR(17));
                    break;
                case '>':
                    mvwaddch(stdscr, y, x, '>');
                    break;
                case '<':
                    mvwaddch(stdscr, y, x, '<');
                    break;
                case '#':
                    attron(COLOR_PAIR(16));
                    mvwaddch(stdscr, y, x, '8');
                    attroff(COLOR_PAIR(16));
                    break;
                default:
                    mvwaddch(stdscr, y, x, ' ');
            }
        }
    }
}

void display_player(player_t *player) {
    mvwprintw(stdscr, 1, 1, "Level %d", player->level);
    mvwprintw(stdscr, 2, 1, "Gold  %d", player->score);
    
    if (player->head.health >= 50) {
        attron(COLOR_PAIR(17));
        mvwaddch(stdscr, 4, 5, 'o');
        attroff(COLOR_PAIR(17));
    } else {
        if (player->head.health >= 25) {
            attron(COLOR_PAIR(49));
            mvwaddch(stdscr, 4, 5, 'o');
            attroff(COLOR_PAIR(49));
        } else {
            if (player->head.health >= 0) {
                attron(COLOR_PAIR(9));
                mvwaddch(stdscr, 4, 5, 'o');
                attroff(COLOR_PAIR(9));
            } else {
                if (player->head.health < 0) {
                    player->head.state = 'r';
                }
            }
        }
    }
    
    if (player->body.health >= 50) {
        attron(COLOR_PAIR(17));
        mvwaddch(stdscr, 5, 5, 'O');
        attroff(COLOR_PAIR(17));
    } else {
        if (player->body.health >= 25) {
            attron(COLOR_PAIR(49));
            mvwaddch(stdscr, 5, 5, 'O');
            attroff(COLOR_PAIR(49));
        } else {
            if (player->body.health >= 0) {
                attron(COLOR_PAIR(9));
                mvwaddch(stdscr, 5, 5, 'O');
                attroff(COLOR_PAIR(9));
            } else {
                if (player->body.health < 0) {
                    player->body.state = 'r';
                }
            }
        }
    }
    
    if (player->l_arm.health >= 50) {
        attron(COLOR_PAIR(17));
        mvwaddch(stdscr, 5, 4, '/');
        attroff(COLOR_PAIR(17));
    } else {
        if (player->l_arm.health >= 25) {
            attron(COLOR_PAIR(49));
            mvwaddch(stdscr, 5, 4, '/');
            attroff(COLOR_PAIR(49));
        } else {
            if (player->l_arm.health >= 0) {
                attron(COLOR_PAIR(9));
                mvwaddch(stdscr, 5, 4, '/');
                attroff(COLOR_PAIR(9));
            } else {
                if (player->l_arm.health < 0) {
                    player->l_arm.state = 'r';
                }
            }
        }
    }
    
    if (player->r_arm.health >= 50) {
        attron(COLOR_PAIR(17));
        mvwaddch(stdscr, 5, 6, '\\');
        attroff(COLOR_PAIR(17));
    } else {
        if (player->r_arm.health >= 25) {
            attron(COLOR_PAIR(49));
            mvwaddch(stdscr, 5, 6, '\\');
            attroff(COLOR_PAIR(49));
        } else {
            if (player->r_arm.health >= 0) {
                attron(COLOR_PAIR(9));
                mvwaddch(stdscr, 5, 6, '\\');
                attroff(COLOR_PAIR(9));
            } else {
                if (player->r_arm.health < 0) {
                    player->r_arm.state = 'r';
                }
            }
        }
    }
    
    if (player->l_leg.health >= 50) {
        attron(COLOR_PAIR(17));
        mvwaddch(stdscr, 6, 4, '/');
        attroff(COLOR_PAIR(17));
    } else {
        if (player->l_leg.health >= 25) {
            attron(COLOR_PAIR(49));
            mvwaddch(stdscr, 6, 4, '/');
            attroff(COLOR_PAIR(49));
        } else {
            if (player->l_leg.health >= 0) {
                attron(COLOR_PAIR(9));
                mvwaddch(stdscr, 6, 4, '/');
                attroff(COLOR_PAIR(9));
            } else {
                if (player->l_leg.health < 0) {
                    player->l_leg.state = 'r';
                }
            }
        }
    }
    
    if (player->r_leg.health >= 50) {
        attron(COLOR_PAIR(17));
        mvwaddch(stdscr, 6, 6, '\\');
        attroff(COLOR_PAIR(17));
    } else {
        if (player->r_leg.health >= 25) {
            attron(COLOR_PAIR(49));
            mvwaddch(stdscr, 6, 6, '\\');
            attroff(COLOR_PAIR(49));
        } else {
            if (player->r_leg.health >= 0) {
                attron(COLOR_PAIR(9));
                mvwaddch(stdscr, 6, 6, '\\');
                attroff(COLOR_PAIR(9));
            } else {
                if (player->r_leg.health < 0) {
                    player->r_leg.state = 'r';
                }
            }
        }
    }

    mvwprintw(stdscr, 8, 1, "Potions : %d", player->potions);
    mvwprintw(stdscr, 9, 1, "Bandages: %d", player->bandages);

    if (player->head.state == 'p' || player->body.state == 'p' || player->l_arm.state == 'p' || player->r_arm.state == 'p' || player->l_leg.state == 'p' || player->r_leg.state == 'p') {
        attron(COLOR_PAIR(17));
        mvwprintw(stdscr,12, 1, "Poison");
        attroff(COLOR_PAIR(17));
    }
    
    if (player->head.state == 'f' || player->body.state == 'f' || player->l_arm.state == 'f' || player->r_arm.state == 'f' || player->l_leg.state == 'f' || player->r_leg.state == 'f') {
        attron(COLOR_PAIR(9));
        mvwprintw(stdscr,13, 1, "Fire Burn");
        attroff(COLOR_PAIR(9));
    }

    if (player->head.state == 'm' || player->body.state == 'm' || player->l_arm.state == 'm' || player->r_arm.state == 'm' || player->l_leg.state == 'm' || player->r_leg.state == 'm') {
        attron(COLOR_PAIR(33));
        mvwprintw(stdscr,14, 1, "Acid Burn");
        attroff(COLOR_PAIR(33));
    }

    if (player->head.state == 'b' || player->body.state == 'b' || player->l_arm.state == 'b' || player->r_arm.state == 'b' || player->l_leg.state == 'b' || player->r_leg.state == 'b') {
        attron(COLOR_PAIR(49));
        mvwprintw(stdscr,15, 1, "Bleed");
        attroff(COLOR_PAIR(49));
    }

    switch ((*player->current_level)[player->posY][player->posX]) {
        case 'A':
            attron(COLOR_PAIR(3));
            mvwaddch(stdscr, player->posY + 2, player->posX + (COLS - 52), '@');
            attroff(COLOR_PAIR(3));
            break;
        case 'B':
            attron(COLOR_PAIR(2));
            mvwaddch(stdscr, player->posY + 2, player->posX + (COLS - 52), '@');
            attroff(COLOR_PAIR(2));
            break;
        case 'C':
            attron(COLOR_PAIR(8));
            mvwaddch(stdscr, player->posY + 2, player->posX + (COLS - 52), '@');
            attroff(COLOR_PAIR(8));
            break;
        case 'D':
            attron(COLOR_PAIR(5));
            mvwaddch(stdscr, player->posY + 2, player->posX + (COLS - 52), '@');
            attroff(COLOR_PAIR(5));
            break;
        default:
            mvwaddch(stdscr, player->posY + 2, player->posX + (COLS - 52), '@');
    }
}

static char *head_state(player_t *player) {
    static char text[10];
    
    memset(text, 0, 10);
    if (player->head.health > 50) {
        sprintf(text, "good");
    } else {
        if (player->head.health > 25) {
            sprintf(text, "mediocre");
        } else {
            if (player->head.health >  0) {
                sprintf(text, "poor");
            } else {
                sprintf(text, "dead"); 
            }
        }
    }
    
    return text;
}

static char *body_state(player_t *player) {
    static char text[10];
    
    memset(text, 0, 10);
    if (player->body.health > 50) {
        sprintf(text, "good");
    } else {
        if (player->body.health > 25) {
            sprintf(text, "mediocre");
        } else {
            if (player->body.health >  0) {
                sprintf(text, "poor");
            } else {
                sprintf(text, "dead"); 
            }
        }
    }
    
    return text;
}

static char *l_arm_state(player_t *player) {
    static char text[10];
    
    memset(text, 0, 10);
    if (player->l_arm.health > 50) {
        sprintf(text, "good");
    } else {
        if (player->l_arm.health > 25) {
            sprintf(text, "mediocre");
        } else {
            if (player->l_arm.health >  0) {
                sprintf(text, "poor");
            } else {
                sprintf(text, "dead"); 
            }
        }
    }
    
    return text;
}

static char *r_arm_state(player_t *player) {
    static char text[10];
    
    memset(text, 0, 10);
    if (player->r_arm.health > 50) {
        sprintf(text, "good");
    } else {
        if (player->r_arm.health > 25) {
            sprintf(text, "mediocre");
        } else {
            if (player->r_arm.health >  0) {
                sprintf(text, "poor");
            } else {
                sprintf(text, "dead"); 
            }
        }
    }
    
    return text;
}

static char *l_leg_state(player_t *player) {
    static char text[10];
    
    memset(text, 0, 10);
    if (player->l_leg.health > 50) {
        sprintf(text, "good");
    } else {
        if (player->l_leg.health > 25) {
            sprintf(text, "mediocre");
        } else {
            if (player->l_leg.health >  0) {
                sprintf(text, "poor");
            } else {
                sprintf(text, "dead"); 
            }
        }
    }
    
    return text;
}

static char *r_leg_state(player_t *player) {
    static char text[10];
    
    memset(text, 0, 10);
    if (player->r_leg.health > 50) {
        sprintf(text, "good");
    } else {
        if (player->r_leg.health > 25) {
            sprintf(text, "mediocre");
        } else {
            if (player->r_leg.health >  0) {
                sprintf(text, "poor");
            } else {
                sprintf(text, "dead"); 
            }
        }
    }
    
    return text;
}

void display_good_ending(player_t *player) {
    clear();
    mvwprintw(stdscr, 5, COLS / 2 - strlen("Congratulation! You have escaped Alive") / 2, "Congratulation! You have escaped Alive");
    mvwprintw(stdscr, 7, 10, "Score:\t%04d points", player->score + 1000);
    mvwprintw(stdscr, 9, 10, "Your head is in a %s state", head_state(player));
    mvwprintw(stdscr,10, 10, "Your body is in a %s state", body_state(player));
    mvwprintw(stdscr,11, 10, "Your left arm is in a %s state", l_arm_state(player));
    mvwprintw(stdscr,12, 10, "Your right arm is in a %s state", r_arm_state(player));
    mvwprintw(stdscr,13, 10, "Your left leg is in a %s state", l_leg_state(player));
    mvwprintw(stdscr,14, 10, "Your right leg is in a %s state", r_leg_state(player));
    getch();
    display_destroy();
    player_destroy(&player);
    exit(EXIT_SUCCESS);
}

void display_bad_ending(player_t *player) {
    if (player->mortal == 1 && (player->head.state == 'r' || player->body.state == 'r')) {
        clear();
        mvwprintw(stdscr, 5, COLS / 2 - strlen("Congratulation? You are Alive!") / 2, "Congratulation? You are Alive!");
        mvwprintw(stdscr, 6, COLS / 2 - strlen("But now you are dead") / 2, "But now you are dead");
        mvwprintw(stdscr, 8, 10, "Score:\t%04d points", player->score + 250);
        mvwprintw(stdscr,10, 10, "Your head was in a %s state", head_state(player));
        mvwprintw(stdscr,11, 10, "Your body was in a %s state", body_state(player));
        mvwprintw(stdscr,12, 10, "Your left arm was in a %s state", l_arm_state(player));
        mvwprintw(stdscr,13, 10, "Your right arm was in a %s state", r_arm_state(player));
        mvwprintw(stdscr,14, 10, "Your left leg was in a %s state", l_leg_state(player));
        mvwprintw(stdscr,15, 10, "Your right leg was in a %s state", r_leg_state(player));
        getch();
        display_destroy();
        player_destroy(&player);
        exit(EXIT_SUCCESS);
    }
}
