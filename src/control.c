#include "control.h"

int key_event(player_t *player, int key) {
    int nothing = 0;
    
    switch(key) {
        default:
            nothing = 1;
            break;
        case 'z':
        case 'w':
        case 'k':
        case KEY_UP:
            walk_up(player);
            break;
        case 'q':
        case 'a':
        case 'h':
        case KEY_LEFT:
            walk_left(player);
            break;
        case 'd':
        case 'l':
        case KEY_RIGHT:
            walk_right(player);
            break;
        case 's':
        case 'j':
        case KEY_DOWN:
            walk_down(player);
            break;
        case '>':
            go_downstair(player);
            break;
        case '<':
            go_upstair(player);
            break;
        case 'p':
            drink_potion(player);
            break;
        case 'b':
            use_bandage(player);
            break;
    }
    
    return nothing;
}