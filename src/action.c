#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "action.h"

static int can_move(player_t *player) {
    return (player->head.state != 'r' || player->body.state != 'r' || player->l_arm.state != 'r' || player->r_arm.state != 'r' || player->l_leg.state != 'r' || player->r_leg.state != 'r');
}

static int can_walk(player_t *player, int offY, int offX) {
        return (((*player->current_level)[player->posY + offY][player->posX + offX] != '0') &&
                ((*player->current_level)[player->posY + offY][player->posX + offX] != '1') &&
                ((*player->current_level)[player->posY + offY][player->posX + offX] != '2'));
}

static int can_drink(player_t *player) {
    return (player->head.state != 'r');
}

static int can_band(player_t *player) {
    return (player->l_arm.state != 'r' || player->r_arm.state != 'r');
}

static int can_go_up(player_t *player) {
    return (player->l_arm.state != 'r' || player->r_arm.state != 'r' || player->l_leg.state != 'r' || player->r_leg.state != 'r');
}

void walk_up(player_t *player) {
    if (can_move(player) == 1) {
        if (can_walk(player, -1, 0) == 1) {
            player->posY -= 1;
        }
    } else {
        set_log("You can not move, you are stuck here forever...");
    }
}

void walk_left(player_t *player) {
    if (can_move(player) == 1) {
        if (can_walk(player, 0, -1) == 1) {
            player->posX -= 1;
        }
    } else {
        set_log("You can not move, you are stuck here forever...");
    }
}

void walk_right(player_t *player) {
    if (can_move(player) == 1) {
        if (can_walk(player, 0, 1) == 1) {
            player->posX += 1;
        }
    } else {
        set_log("You can not move, you are stuck here forever...");
    }
}

void walk_down(player_t *player) {
    if (can_move(player) == 1) {
        if (can_walk(player, 1, 0) == 1) {
            player->posY += 1;
        }
    } else {
        set_log("You can not move, you are stuck here forever...");
    }
}

void drink_potion(player_t *player) {
    if (player->potions > 0) {
        if (can_drink(player) == 1) {
            if (player->head.state != 'r') player->head.health += 25;
            if (player->body.state != 'r') player->body.health += 25;
            if (player->l_arm.state != 'r') player->l_arm.health += 25;
            if (player->r_arm.state != 'r') player->r_arm.health += 25;
            if (player->l_leg.state != 'r') player->l_leg.health += 25;
            if (player->r_leg.state != 'r') player->r_leg.health += 25;
            player->potions -= 1;
        } else {
            set_log("You can not drink without at least your head.");
        }
    } else {
        set_log("You do not have any potion.");
    }
}

void use_bandage(player_t *player) {
    if (player->bandages > 0) {
        if (can_band(player) == 1) {
            if (player->head.state != 'r') player->head.state = 'o';
            if (player->body.state != 'r') player->body.state = 'o';
            if (player->l_arm.state != 'r') player->l_arm.state = 'o';
            if (player->r_arm.state != 'r') player->r_arm.state = 'o';
            if (player->l_leg.state != 'r') player->l_leg.state = 'o';
            if (player->r_leg.state != 'r') player->r_leg.state = 'o';
            player->bandages -= 1;
        } else {
            set_log("You can not use a bandage without at least an arm.");
        }
    } else {
        set_log("You do not have any bandages.");
    }
}

void go_downstair(player_t *player) {
    if (can_move(player) == 1) {
        if ((*player->current_level)[player->posY][player->posX] == '>') {
            if (player->posY == 3 && player->posX == 43) { player->current_level = &level_02; }
            if (player->posY == 6 && player->posX == 9) { player->current_level = &level_03; }
            if (player->posY == 14 && player->posX == 42) { player->current_level = &level_04; }
            if (player->posY == 8 && player->posX == 8) { player->current_level = &level_05; }
            player->level += 1;
        }
    }
}

void go_upstair(player_t *player) {
    if (can_go_up(player) == 1) {
        if ((*player->current_level)[player->posY][player->posX] == '<') {
            if (player->posY == 5 && player->posX == 5 && player->mortal == 1) {
                set_log("Congratulation, you have exited the temple alive !");
                display_good_ending(player);
            }
            if (player->posY == 3 && player->posX == 43) { player->current_level = &level_01; player->level -= 1; }
            if (player->posY == 6 && player->posX == 9) { player->current_level = &level_02; player->level -= 1; }
            if (player->posY == 14 && player->posX == 42) { player->current_level = &level_03; player->level -= 1; }
            if (player->posY == 8 && player->posX == 8) { player->current_level = &level_04; player->level -= 1; }
        }
    } else {
        set_log("You can not go upstair without an arm or a leg.");
    }
}

void check_floor(player_t *player) {
    switch((*player->current_level)[player->posY][player->posX]) {
        case '5':
        case 'A':
            (*player->current_level)[player->posY][player->posX] = 'A';
            set_log("You activate a poison trap.");
            get_poisoned(player);
            break;
        case '6':
        case 'B':
            (*player->current_level)[player->posY][player->posX] = 'B';
            set_log("You activate a fire trap.");
            get_burned(player);
            break;
        case '7':
        case 'C':
            (*player->current_level)[player->posY][player->posX] = 'C';
            set_log("You activate a spear trap.");
            get_pierced(player);
            break;
        case '8':
        case 'D':
            (*player->current_level)[player->posY][player->posX] = 'D';
            set_log("You activate an acid trap.");
            get_acid_burn(player);
            break;
        case '9':
        case 'E':
            (*player->current_level)[player->posY][player->posX] = 'E';
            set_log("You fell in a shaft.");
            fall(player);
            break;
        case '$':
            (*player->current_level)[player->posY][player->posX] = '3';
            if (player->mortal) { player->score += 20; } else { player->score += 10; }
            break;
        case '!':
            (*player->current_level)[player->posY][player->posX] = '3';
            set_log("You found a potion.");
            player->potions += 1;
            break;
        case '=':
            (*player->current_level)[player->posY][player->posX] = '3';
            set_log("You found a bandage.");
            player->bandages += 1;
            break;
        case '#':
            (*player->current_level)[player->posY][player->posX] = '3';
            set_log("You become mortal. Congratulation !");
            player->score += 500;
            player->mortal = 1;
            break;
    }
}

void get_poisoned(player_t *player) {
    if (player->head.state != 'r') player->head.state = 'p';
    if (player->body.state != 'r') player->body.state = 'p';
}

void get_burned(player_t *player) {
    if ((player->l_leg.state == 'r') && (player->r_leg.state == 'r')) {
        if (player->body.state != 'r') player->body.state = 'f';
        if (player->head.state != 'r') player->head.state = 'f';
        if (player->l_arm.state != 'r') player->l_arm.state = 'f';
        if (player->r_arm.state != 'r') player->r_arm.state = 'f';
    } else {
        if (player->l_leg.state != 'r') player->l_leg.state = 'f';
        if (player->r_leg.state != 'r') player->r_leg.state = 'f';
    }
}

void get_pierced(player_t *player) {
    switch (rand() % 6) {
        case 0:
            player->head.health -= 15;
            if (player->head.state != 'r') player->head.state = 'b';
            break;
        case 1:
            player->body.health -= 15;
            if (player->body.state != 'r') player->body.state = 'b';
            break;
        case 2:
            player->l_arm.health -= 15;
            if (player->l_arm.state != 'r') player->l_arm.state = 'b';
            break;
        case 3:
            player->r_arm.health -= 15;
            if (player->r_arm.state != 'r') player->r_arm.state = 'b';
            break;
        case 4:
            player->l_leg.health -= 15;
            if (player->l_leg.state != 'r') player->l_leg.state = 'b';
            break;
        case 5:
            player->l_leg.health -= 15;
            if (player->r_leg.state != 'r') player->r_leg.state = 'b';
            break;
    }
}

void get_acid_burn(player_t *player) {
    if (player->head.state != 'r') player->head.state = 'm';
    if (player->body.state != 'r') player->body.state = 'm';
    if (player->l_arm.state != 'r') player->l_arm.state = 'm';
    if (player->r_arm.state != 'r') player->r_arm.state = 'm';
    if (player->l_leg.state != 'r') player->l_leg.state = 'm';
    if (player->r_leg.state != 'r') player->r_leg.state = 'm';
}

void fall(player_t *player) {
    if (player->l_leg.state == 'r' && player->r_leg.state == 'r') {
        if (player->head.state != 'r') player->head.state = 'd';
        if (player->body.state != 'r') player->body.state = 'd';
        if (player->l_arm.state != 'r') player->l_arm.state = 'd';
        if (player->r_arm.state != 'r') player->r_arm.state = 'd';
    } else {
        if (player->l_leg.state != 'r') player->l_leg.state = 'd';
        if (player->r_leg.state != 'r') player->r_leg.state = 'd';
    }
    
    if (player->posY == 11 && player->posX == 34) { player->current_level = &level_02; }
    if (player->posY == 4 && player->posX == 31) { player->current_level = &level_03; }
    if (player->posY == 13 && player->posX == 28) { player->current_level = &level_04; }
    if (player->posY == 13 && player->posX == 34) { player->current_level = &level_04; }
    if (player->posY == 14 && player->posX == 31) { player->current_level = &level_05; }
    player->level += 1;
}

void take_poison_damage(player_t *player) {
    if (player->head.state == 'p') {
        player->head.health -= 5;
        if (rand() % 100 < 40) {
            player->head.state = 'o';
        }
    }

    if (player->body.state == 'p') {
        player->body.health -= 5;
        if (rand() % 100 < 40) {
            player->body.state = 'o';
        }
    }

    if (player->l_arm.state == 'p') {
        player->l_arm.health -= 5;
        if (rand() % 100 < 40) {
            player->l_arm.state = 'o';
        }
    }

    if (player->r_arm.state == 'p') {
        player->r_arm.health -= 5;
        if (rand() % 100 < 40) {
            player->r_arm.state = 'o';
        }
    }

    if (player->l_leg.state == 'p') {
        player->l_leg.health -= 5;
        if (rand() % 100 < 40) {
            player->l_leg.state = 'o';
        }
    }

    if (player->r_leg.state == 'p') {
        player->r_leg.health -= 5;
        if (rand() % 100 < 40) {
            player->r_leg.state = 'o';
        }
    }
}

void take_fire_damage(player_t *player) {
    if (player->head.state == 'f') {
        player->head.health -= 5;
        if (rand() % 100 < 30) {
            player->head.state = 'o';
        }
    }

    if (player->body.state == 'f') {
        player->body.health -= 5;
        if (rand() % 100 < 30) {
            player->body.state = 'o';
        }
    }

    if (player->l_arm.state == 'f') {
        player->l_arm.health -= 5;
        if (rand() % 100 < 30) {
            player->l_arm.state = 'o';
        }
    }

    if (player->r_arm.state == 'f') {
        player->r_arm.health -= 5;
        if (rand() % 100 < 30) {
            player->r_arm.state = 'o';
        }
    }

    if (player->l_leg.state == 'f') {
        player->l_leg.health -= 5;
        if (rand() % 100 < 30) {
            player->l_leg.state = 'o';
        }
    }

    if (player->r_leg.state == 'f') {
        player->r_leg.health -= 5;
        if (rand() % 100 < 30) {
            player->r_leg.state = 'o';
        }
    }
}

void take_spear_damage(player_t *player) {
    if (player->head.state == 'b') {
        player->head.health -= 2;
    }

    if (player->body.state == 'b') {
        player->body.health -= 2;
    }

    if (player->l_arm.state == 'b') {
        player->l_arm.health -= 2;
    }

    if (player->r_arm.state == 'b') {
        player->r_arm.health -= 2;
    }

    if (player->l_leg.state == 'b') {
        player->l_leg.health -= 2;
    }

    if (player->r_leg.state == 'b') {
        player->r_leg.health -= 2;
    }
}

void take_acid_damage(player_t *player) {
    if (player->head.state == 'm') {
        player->head.health -= 10;
        if (rand() % 100 < 10) {
            player->head.state = 'f';
        }
    }

    if (player->body.state == 'm') {
        player->body.health -= 10;
        if (rand() % 100 < 10) {
            player->body.state = 'f';
        }
    }

    if (player->l_arm.state == 'm') {
        player->l_arm.health -= 10;
        if (rand() % 100 < 10) {
            player->l_arm.state = 'f';
        }
    }

    if (player->r_arm.state == 'm') {
        player->r_arm.health -= 10;
        if (rand() % 100 < 10) {
            player->r_arm.state = 'f';
        }
    }

    if (player->l_leg.state == 'm') {
        player->l_leg.health -= 10;
        if (rand() % 100 < 10) {
            player->l_leg.state = 'f';
        }
    }

    if (player->r_leg.state == 'm') {
        player->r_leg.health -= 10;
        if (rand() % 100 < 10) {
            player->r_leg.state = 'f';
        }
    }
}

void take_fall_damage(player_t *player) {
    if (player->head.state == 'd') {
        player->head.health /= 2;
        player->head.state = 'o';
    }

    if (player->body.state == 'd') {
        player->body.health /= 2;
        player->body.state = 'o';
    }

    if (player->l_arm.state == 'd') {
        player->l_arm.health /= 2;
        player->l_arm.state = 'o';
    }

    if (player->r_arm.state == 'd') {
        player->r_arm.health /= 2;
        player->r_arm.state = 'o';
    }

    if (player->l_leg.state == 'd') {
        player->l_leg.health /= 2;
        player->l_leg.state = 'o';
    }

    if (player->r_leg.state == 'd') {
        player->r_leg.health /= 2;
        player->r_leg.state = 'o';
    }
}

void set_log(const char* text) {
    memset(log_line, 0, 50);
    sprintf(log_line, "%s", text);
}