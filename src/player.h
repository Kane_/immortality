#ifndef HAVE_PLAYER_H
#define HAVE_PLAYER_H

#include "world.h"

typedef struct player_s player_t;
typedef struct body_part_s body_part_t;

/**
  * state:
  * o: OK
  * p: Poison
  * f: Burn (fire)
  * m: Melt (acid)
  * s: Bleed (spear)
  * d: Drop (falling)
  * r: RIP
  *
  */
struct body_part_s {
    int  health;
    char state;
};

struct player_s {
    char         (*current_level)[18][50];
    int          mortal;
    int          level;
    int          score;
    int          potions;
    int          bandages;
    int          posY;
    int          posX;
    body_part_t  head;
    body_part_t  body;
    body_part_t  l_arm;
    body_part_t  r_arm;
    body_part_t  l_leg;
    body_part_t  r_leg;
};

player_t *player_init(void);
void      player_destroy(player_t **player);

#endif
