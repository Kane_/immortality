#include <stdlib.h>
#include <stdio.h>

#include "display.h"
#include "player.h"
#include "control.h"
#include "action.h"

int main(void) {
    int c = 0;
    player_t *player = NULL;
    
    player = player_init();
    display_init();
    display_init_colors();

    display_title();
    getch();
    do {
        if (!key_event(player, c)) {
            check_floor(player);
            take_poison_damage(player);
            take_fire_damage(player);
            take_spear_damage(player);
            take_acid_damage(player);
            take_fall_damage(player);
            display_bad_ending(player);
        }
        display_interface();
        display_log();
        display_map(player->current_level);
        display_player(player);
    } while ((c = getch()) != 27);
    
    display_destroy();
    player_destroy(&player);
    
    return EXIT_SUCCESS;
}
